<?php

    // Inisialisasi Variabel
    $host = "localhost";
    $username = "root";
    $password = "";
    $db_name = "sim-dosen";
    
    // Mencoba konek ke database
    try {
        $conn = mysqli_connect($host, $username, $password, $db_name);
    } catch (error) {
        echo "Koneksi Gagal: " . $error;
    }

    // Function tampilkan data dari database
    function query($query) {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];

        // Memasukkan data result ke dalam variabel rows
        while($row = mysqli_fetch_assoc($result)){
            $rows[] = $row;
        }
 
        return $rows; //Return data rows
    }

    // Function tambah_dosen
    function tambah_dosen($data){
        global $conn;

        // Inisialisasi variabel inputan
        $nip = $data['nip_dosen'];
        $nama = $data['nama_dosen'];
        $prodi = $data['prodi'];
        $fakultas = $data['fakultas'];
        $foto = $_FILES['foto_dosen']['name'];

        // Memindahkan foto ke dalam directory local
        $store_foto = rand(1,100)."-".$_FILES['foto_dosen']['name']; //Mengubah nama foto sesuai format
        $temporary_name = $_FILES['foto_dosen']['tmp_name']; //Mengambil data temporary name 
        $upload_dirr = "../assets/foto-dosen/".$store_foto; //Inisialisasi lokasi penyimpanan file        
        
        move_uploaded_file($temporary_name, $upload_dirr); //Store file

        $query = "INSERT INTO dosen VALUES('', '$store_foto', '$nip', '$nama', '$prodi', '$fakultas' )"; //Query SQL
        mysqli_query($conn, $query); //Insert data ke dalam database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);
        return $result;

    }

    // Function hapus dosen
    function hapus_dosen($id){
        global $conn;

        $query = "DELETE FROM dosen WHERE id_dosen = '$id'"; //Query SQL
        mysqli_query($conn, $query); //Hapus data dari database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);
        return $result;
    }

    // Function ubah dosen
    function ubah_dosen($data, $id){
        global $conn;

        // Inisialisasi data inputan
        $nip = $data['nip_dosen'];
        $nama = $data['nama_dosen'];
        $prodi = $data['prodi'];
        $fakultas = $data['fakultas'];
        $foto = $_FILES['foto_dosen']['name'];

        // Pengecekan apakah user mengupload gambar atau tidak
        if(empty($foto)){
            // Jika tidak maka gambar tidak diupdate
            $query = "UPDATE dosen SET nip_dosen='$nip', nama_dosen='$nama', prodi='$prodi', fakultas='$fakultas' WHERE id_dosen='$id'"; //Query SQL            
        } else {
            // Jika iya maka gambar akan diupdate
            // Memindahkan foto ke dalam directory local
            $store_foto = rand(1,100)."-".$_FILES['foto_dosen']['name']; //Mengubah nama foto sesuai format
            $temporary_name = $_FILES['foto_dosen']['tmp_name']; //Mengambil data temporary name 
            $upload_dirr = "../assets/foto-dosen/".$store_foto; //Inisialisasi lokasi penyimpanan file        

            move_uploaded_file($temporary_name, $upload_dirr); //Store file

            $query = "UPDATE dosen SET foto_dosen='$store_foto', nip_dosen='$nip', nama_dosen='$nama', prodi='$prodi', fakultas='$fakultas' WHERE id_dosen='$id'"; //Query SQL            
        }        
        mysqli_query($conn, $query); //Insert data ke dalam database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);                
        return $result;
    }

    // Function tambah kelas
    function tambah_kelas($data){
        global $conn;
        
        // Inisialisasi variabel inputan
        $nama = $data['nama_kelas'];
        $prodi = $data['prodi'];
        $fakultas = $data['fakultas'];                                  

        $query = "INSERT INTO kelas VALUES('', '$nama', '$prodi', '$fakultas' )"; //Query SQL
        mysqli_query($conn, $query); //Insert data ke dalam database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);
        return $result;

    }

    // Function hapus kelas
    function hapus_kelas($id){
        global $conn;

        $query = "DELETE FROM kelas WHERE id_kelas = '$id'"; //Query SQL
        mysqli_query($conn, $query); //Hapus data dari database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);
        return $result;
    }

    // Function ubah kelas
    function ubah_kelas($data, $id){
        global $conn;

        // Inisialisasi variabel inputan
        $nama = $data['nama_kelas'];
        $prodi = $data['prodi'];
        $fakultas = $data['fakultas']; 

        $query = "UPDATE kelas SET nama_kelas='$nama', prodi='$prodi', fakultas='$fakultas' WHERE id_kelas='$id'"; //Query SQL      
        mysqli_query($conn, $query); //Insert data ke dalam database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);                
        return $result;
    }

    // Function tambah jadwal
    function tambah_jadwal($data){
        global $conn;
        
        // Inisialisasi variabel inputan
        $dosen = $data['dosen_pengampu'];
        $kelas = $data['kelas'];
        $matkul = $data['mata_kuliah'];                                  
        $jadwal = $data['jadwal_kuliah'];                   

        $query = "INSERT INTO jadwal_kelas VALUES('', '$dosen', '$kelas', '$jadwal', '$matkul' )"; //Query SQL
        mysqli_query($conn, $query); //Insert data ke dalam database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);
        return $result;
    }

    // Function hapus kelas
    function hapus_jadwal($id){
        global $conn;

        $query = "DELETE FROM jadwal_kelas WHERE id_jadwal = '$id'"; //Query SQL
        mysqli_query($conn, $query); //Hapus data dari database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);
        return $result;
    }

    // Function ubah jadwal
    function ubah_jadwal($data, $id){
        global $conn;

        // Inisialisasi variabel inputan
        // Inisialisasi variabel inputan
        $dosen = $data['dosen_pengampu'];
        $kelas = $data['kelas'];
        $matkul = $data['mata_kuliah'];                                  
        $jadwal = $data['jadwal_kuliah']; 

        $query = "UPDATE jadwal_kelas SET id_dosen='$dosen', id_kelas='$kelas', jadwal='$jadwal', mata_kuliah='$matkul' WHERE id_jadwal='$id'"; //Query SQL      
        mysqli_query($conn, $query); //Insert data ke dalam database

        // Membuat variabel result, jika gagal akan mengembalikan nilai -1, jika berhasil akan mengembalikan nilai 1
        $result = mysqli_affected_rows($conn);                
        return $result;
    }

?>