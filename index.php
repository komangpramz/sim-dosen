<?php

    require 'config/function.php';

    // Query mengambil total data dosen dalam tabel
    $queryDosen = "SELECT COUNT(*) as total_dosen FROM dosen";
    $t_dosen = query($queryDosen)[0]; //Inisialisasi variabel total dosen

    // Query mengambil total data kelas dalam tabel
    $queryKelas = "SELECT COUNT(*) as total_kelas FROM kelas";
    $t_kelas = query($queryKelas)[0]; //Inisialisasi variabel total kelas

    // Query mengambil total data jadwal dalam tabel
    $queryJadwal = "SELECT COUNT(*) as total_jadwal FROM jadwal_kelas";
    $t_jadwal = query($queryJadwal)[0]; //Inisialisasi variabel total jadwal

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="resource/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css">

    <title>Beranda</title>
</head>

<body>

    <!-- NAVIGATION BAR -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-right">
        <a class="navbar-brand" href="index.php">SIM-DOSEN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse right" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="pages/dosen.php">Dosen</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="pages/kelas.php">Kelas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="pages/jadwal.php">Jadwal</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="divider"></div>

    <!-- CONTAINER -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Dashboard Sistem Informasi Manajemen Dosen </h3>

                <div class="divider"></div>

                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Data Dosen</h5>
                                <div class="card-text mb-16">
                                    <i class="fas fa-chalkboard-teacher"></i>
                                    <span>Total: <?= $t_dosen['total_dosen'];?> Orang</span>
                                </div>
                                <a href="pages/dosen.php" class="btn btn-primary">Lihat Lebih Lanjut</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Data Kelas</h5>
                                <div class="card-text mb-16">
                                    <i class="fas fa-door-open"></i>
                                    <span>Total: <?= $t_kelas['total_kelas'];?> Kelas</span>
                                </div>
                                <a href="pages/kelas.php" class="btn btn-primary">Lihat Lebih Lanjut</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Data Jadwal</h5>
                                <div class="card-text mb-16">
                                    <i class="fas fa-calendar-alt"></i>    
                                    <span>Total: <?= $t_jadwal['total_jadwal'];?> Jadwal</span>
                                </div>
                                <a href="pages/jadwal.php" class="btn btn-primary">Lihat Lebih Lanjut</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>


</body>

</html>