<?php
    require '../config/function.php';

     // Ambil data jadwal dari URL
     $id = $_GET['id_jadwal'];

    //  Query data dosen
     $queryDosen = "SELECT * FROM dosen ORDER BY id_dosen ASC";
     $dosen = query($queryDosen);
 
    //  Query data kelas
     $queryKelas = "SELECT * FROM kelas ORDER BY id_kelas ASC";
     $kelas = query($queryKelas);

     // Query data jadwal berdasarkan id
     $query = "SELECT id_jadwal, nama_dosen, id_dosen, nama_kelas, id_kelas, jadwal, mata_kuliah FROM jadwal_kelas 
     INNER JOIN dosen USING(id_dosen)
     INNER JOIN kelas USING(id_kelas) WHERE id_jadwal='$id'";
     $jadwal = query($query)[0];     

    if(isset($_POST['submit'])) {                     
                                                  
        // KONDISI DATA BERHASIL DITAMBAHKAN
        if(ubah_jadwal($_POST, $jadwal['id_jadwal']) > 0) {                        
            echo "
            <script>
                alert('Data Jadwal Berhasil Diubah!');
                document.location.href='jadwal.php';
            </script>
        ";     
        // KONDISI DATA GAGAL DITAMBAHKAN
        } else {                                
            echo "
            <script>
                alert('Data Jadwal Gagal Diubah!');
                document.location.href='jadwal.php';
            </script>
        ";                    
        }                                                                  
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resource/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <title>Halaman Ubah Jadwal</title>
</head>

<body>

    <!-- NAVIGATION BAR -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-right">
        <a class="navbar-brand" href="../index.php">SIM-DOSEN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse right" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Beranda</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="dosen.php">Dosen</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="kelas.php">Kelas</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="jadwal.php">Jadwal</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="divider"></div>

    <!-- CONTAINER -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Ubah Data Jadwal </h3>

                <div class="divider"></div>

                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="">                                                      
                            <div class="form-group">
                                <label for="dosen_pengampu">Dosen Pengampu</label>
                                <select id="dosen_pengampu" name="dosen_pengampu" class="form-control">
                                    <option selected>- Silahkan Pilih Dosen -</option>
                                    <?php
                                        foreach($dosen as $d):
                                    ?>
                                        <option <?= ($jadwal['id_dosen'] == $d['id_dosen'] ? "selected" : "")?> value="<?= $d['id_dosen'];?>"><?= $d['nama_dosen']; ?> - <?= $d['fakultas']?></option>
                                    <?php endforeach ?>                                    
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="mata_kuliah">Mata Kuliah</label>
                                <input value="<?= $jadwal['mata_kuliah'] ?>" required type="text" name="mata_kuliah" id="mata_kuliah" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="jadwal_kuliah">Jadwal Kuliah</label>
                                <div class='input-group date' id='datetimepicker'>
                                    <input value="<?= $jadwal['jadwal'] ?>" required type='text' class="form-control" name="jadwal_kuliah">
                                    <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>                                
                            </div>
                            <div class="form-group">
                                <label for="kelas">Kelas</label>
                                <select id="kelas" name="kelas" class="form-control">
                                    <option selected>- Silahkan Pilih Kelas -</option>
                                    <?php
                                        foreach($kelas as $k):
                                    ?>
                                        <option <?= ($jadwal['id_kelas'] == $k['id_kelas'] ? "selected" : "")?>  value="<?= $k['id_kelas'] ?>"><?= $k['nama_kelas'] ?></option>
                                    <?php endforeach ?>                                   
                                </select>
                            </div>                           
                                    
                            <div class="divider"></div>
                            <button name="submit" type="submit" class="btn btn-primary"><i class="fas fa-check"></i> Ubah Data Jadwal</button>
                            
                        </form>
                    </div>
                </div>               

            </div>
        </div>
    </div>    

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">        
        $(function(){
            $("#datetimepicker").datetimepicker({
                format: 'YYYY-MM-DD hh:mm:ss'
            });
        });
    </script>


</body>

</html>