<?php    
    require '../config/function.php';

    // Ambil data dari id URL
    $id = $_GET['id_kelas'];    

    // Query data kelas berdasarkan id
    $query = "SELECT * FROM kelas WHERE id_kelas='$id'";
    $kelas = query($query)[0];

    if(isset($_POST['submit'])) {                    
                                
    // KONDISI DATA BERHASIL DITAMBAHKAN
        if(ubah_kelas($_POST, $kelas['id_kelas']) > 0) {                        
            echo "
            <script>
                alert('Data Kelas Berhasil Diubah!');
                document.location.href='kelas.php';
            </script>
        ";     
    // KONDISI DATA GAGAL DITAMBAHKAN
        } else {                                
            echo "
            <script>
                alert('Data Kelas Gagal Diubah!');
                document.location.href='kelas.php';
            </script>
        ";                    
        }                                                                  
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="../resource/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css">
    <title>Halaman Ubah Kelas</title>
</head>

<body>

    <!-- NAVIGATION BAR -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-right">
        <a class="navbar-brand" href="../index.php">SIM-DOSEN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse right" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Beranda</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="dosen.php">Dosen</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="kelas.php">Kelas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="jadwal.php">Jadwal</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="divider"></div>

    <!-- CONTAINER -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Ubah Data Kelas </h3>

                <div class="divider"></div>

                <div class="row">

                    <!-- FORM -->
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <form method="POST" action="">
                                    <div class="form-group">
                                        <label for="nama_kelas">Nama Kelas</label>
                                        <input type="text" class="form-control" id="nama_kelas" name="nama_kelas" value="<?= $kelas['nama_kelas']?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="prodi">Program Studi</label>
                                        <select required id="prodi" name="prodi" class="form-control">
                                            <option>- Silahkan Pilih Program Studi -</option>
                                            <option <?= ($kelas['prodi'] == 'Sistem Informasi' ? "selected" : "")  ?> value="Sistem Informasi">Sistem Informasi</option>
                                            <option <?= ($kelas['prodi'] == 'Pendidikan Teknik Informatika' ? "selected" : "")  ?> value="Pendidikan Teknik Informatika">Pendidikan Teknik Informatika
                                            </option>
                                            <option <?= ($kelas['prodi'] == 'Manajemen Informatika' ? "selected" : "")  ?> value="Manajemen Informatika">Manajemen Informatika</option>
                                            <option <?= ($kelas['prodi'] == 'Ilmu Komputer' ? "selected" : "")  ?> value="Ilmu Komputer">Ilmu Komputer</option>
                                            <option <?= ($kelas['prodi'] == 'Ilmu Hukum' ? "selected" : "")  ?> value="Ilmu Hukum">Ilmu Hukum</option>
                                            <option <?= ($kelas['prodi'] == 'Manajemen' ? "selected" : "")  ?> value="Manajemen">Manejemen</option>
                                            <option <?= ($kelas['prodi'] == 'Pendidikan Matematika' ? "selected" : "")  ?> value="Pendidikan Matematika">Pendidikan Matematika</option>
                                            <option <?= ($kelas['prodi'] == 'Kedokteran' ? "selected" : "")  ?> value="Kedokteran">Kedokteran</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="fakultas">Fakultas</label>
                                        <select required id="fakultas" name="fakultas" class="form-control">
                                            <option>- Silahkan Pilih Fakultas -</option>
                                            <option <?= ($kelas['fakultas'] == 'Teknik dan Kejuruan' ? "selected" : "")  ?> value="Teknik dan Kejuruan">Teknik dan Kejuruan</option>
                                            <option <?= ($kelas['fakultas'] == 'Matematika dan Ilmu Pengetahuan Alam' ? "selected" : "")  ?> value="Matematika dan Ilmu Pengetahuan Alam">Matematika dan Ilmu
                                                Pengetahuan Alam
                                            </option>
                                            <option <?= ($kelas['fakultas'] == 'Ekonomi' ? "selected" : "")  ?> value="Ekonomi">Ekonomi</option>
                                            <option <?= ($kelas['fakultas'] == 'Hukum dan Ilmu Sosial' ? "selected" : "")  ?> value="Hukum dan Ilmu Sosial">Hukum dan Ilmu Sosial</option>
                                            <option <?= ($kelas['fakultas'] == 'Kedokteran' ? "selected" : "")  ?> value="Kedokteran">Kedokteran</option>
                                        </select>
                                    </div>

                                    <div class="divider"></div>
                                    <button type="submit" name="submit" class="btn btn-primary"><i
                                            class="fas fa-check"></i> Ubah Data Kelas</button>

                                </form>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>


</body>

</html>