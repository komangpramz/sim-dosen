<?php

    require '../config/function.php';

    $id = $_GET['id_dosen'];
    
    if(hapus_dosen($id) > 0){
        echo "
            <script>
                alert('Data Dosen Berhasil Dihapus!');
                document.location.href = 'dosen.php';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('Data Dosen Gagal Dihapus!');
                document.location.href = 'dosen.php';
            </script>
        ";
    }

?>