<?php

    require '../config/function.php';

    $id = $_GET['id_jadwal'];
    
    if(hapus_jadwal($id) > 0){
        echo "
            <script>
                alert('Data Jadwal Berhasil Dihapus!');
                document.location.href = 'jadwal.php';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('Data Jadwal Gagal Dihapus!');
                document.location.href = 'jadwal.php';
            </script>
        ";
    }

?>