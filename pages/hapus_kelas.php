<?php

    require '../config/function.php';

    $id = $_GET['id_kelas'];
    
    if(hapus_kelas($id) > 0){
        echo "
            <script>
                alert('Data Kelas Berhasil Dihapus!');
                document.location.href = 'kelas.php';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('Data Kelas Gagal Dihapus!');
                document.location.href = 'kelas.php';
            </script>
        ";
    }

?>