<?php
    require '../config/function.php';

    // Query select data jadwal dengan inner join tabel dosen dan kelas
    $query = "SELECT id_jadwal, nama_dosen, nama_kelas, jadwal, mata_kuliah FROM jadwal_kelas 
    INNER JOIN dosen USING(id_dosen)
    INNER JOIN kelas USING(id_kelas)
    ORDER BY id_jadwal ASC";
    $jadwal = query($query); //Memanggil function query

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="../resource/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css">
    <title>Halaman Jadwal</title>
</head>

<body>

    <!-- NAVIGATION BAR -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-right">
        <a class="navbar-brand" href="../index.php">SIM-DOSEN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse right" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="dosen.php">Dosen</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="kelas.php">Kelas</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="jadwal.php">Jadwal</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="divider"></div>

    <!-- CONTAINER -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Data Jadwal </h3>

                <div class="divider"></div>

                <a href="tambah_jadwal.php" class="btn btn-primary">
                    <i class="fas fa-plus"></i>
                    Tambah Data Jadwal
                </a>

                <div class="mb-16"></div>

                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col" style="width: 200px">Dosen Pengajar</th>
                                <th scope="col" style="width: 150px">Mata Kuliah</th>
                                <th scope="col">Jadwal Kuliah</th>
                                <th scope="col">Kelas</th>
                                <th scope="col" class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Pengecekan Kondisi Data Tabel Kosong -->
                            <?php
                                if(empty($jadwal)){
                            ?>
                            <td colspan="6" class="empty-row">Data Kosong</td>
                            <?php
                                } else {
                                    $i = 1; //Index number
                                    foreach($jadwal as $j):
                            ?>
                            <tr>
                                <!-- Print Data Jadwal -->
                                <th scope="row"><?= $i ?></th>
                                <td><?= $j['nama_dosen'] ?></td>
                                <td><?= $j['mata_kuliah'] ?></td>
                                <td><?= $j['jadwal'] ?></td>
                                <td><?= $j['nama_kelas'] ?>.</td>
                                <td class="text-center">
                                    <a href="edit_jadwal.php?id_jadwal=<?= $j['id_jadwal'];?>" class="btn btn-success">
                                        <i class="fas fa-edit"></i>
                                        Edit
                                    </a>

                                    <a href="hapus_jadwal.php?id_jadwal=<?= $j['id_jadwal'];?>" class="btn btn-danger" onclick="return confirm('Ingin Menghapus Data Jadwal?')">
                                        <i class="fas fa-trash"></i>
                                        Hapus
                                    </a>
                                </td>
                            </tr>

                            <?php
                                $i++;
                                endforeach;
                                }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>


</body>

</html>