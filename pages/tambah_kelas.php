<?php    
    require '../config/function.php';

    if(isset($_POST['submit'])) {                    
                                                                                        
        // KONDISI DATA BERHASIL DITAMBAHKAN
        if(tambah_kelas($_POST) > 0) {                        
            echo "
            <script>
                alert('Data Kelas Berhasil Ditambah!');
                document.location.href='kelas.php';
            </script>
        ";     
        // KONDISI DATA GAGAL DITAMBAHKAN
        } else {                                
            echo "
            <script>
                alert('Data Kelas Gagal Ditambah!');
                document.location.href='kelas.php';
            </script>
        ";                    
        }                                                                  
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="../resource/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css">
    <title>Halaman Tambah Kelas</title>
</head>

<body>

    <!-- NAVIGATION BAR -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-right">
        <a class="navbar-brand" href="../index.php">SIM-DOSEN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse right" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Beranda</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="dosen.php">Dosen</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="kelas.php">Kelas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="jadwal.php">Jadwal</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="divider"></div>

    <!-- CONTAINER -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Tambah Data Kelas </h3>

                <div class="divider"></div>

                <div class="row">

                    <!-- FORM -->
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <form method="POST" action="">
                                    <div class="form-group">
                                        <label for="nama_kelas">Nama Kelas</label>
                                        <input required type="text" class="form-control" id="nama_kelas" name="nama_kelas">
                                    </div>
                                    <div class="form-group">
                                        <label for="prodi">Program Studi</label>
                                        <select id="prodi" name="prodi" class="form-control">
                                            <option selected>- Silahkan Pilih Program Studi -</option>
                                            <option value="Sistem Informasi">Sistem Informasi</option>
                                            <option value="Pendidikan Teknik Informatika">Pendidikan Teknik Informatika
                                            </option>
                                            <option value="Manajemen Informatika">Manajemen Informatika</option>
                                            <option value="Ilmu Komputer">Ilmu Komputer</option>
                                            <option value="Ilmu Hukum">Ilmu Hukum</option>
                                            <option value="Manajemen">Manejemen</option>
                                            <option value="Pendidikan Matematika">Pendidikan Matematika</option>
                                            <option value="Kedokteran">Kedokteran</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="fakultas">Fakultas</label>
                                        <select required id="fakultas" name="fakultas" class="form-control">
                                            <option selected>- Silahkan Pilih Fakultas -</option>
                                            <option value="Teknik dan Kejuruan">Teknik dan Kejuruan</option>
                                            <option value="Matematika dan Ilmu Pengetahuan Alam">Matematika dan Ilmu
                                                Pengetahuan Alam
                                            </option>
                                            <option value="Ekonomi">Ekonomi</option>
                                            <option value="Hukum dan Ilmu Sosial">Hukum dan Ilmu Sosial</option>
                                            <option value="Kedokteran">Kedokteran</option>
                                        </select>
                                    </div>

                                    <div class="divider"></div>
                                    <button type="submit" name="submit" class="btn btn-primary"><i
                                            class="fas fa-check"></i> Tambah Data Kelas</button>

                                </form>
                            </div>
                        </div>
                    </div>                   
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    </script>


</body>

</html>